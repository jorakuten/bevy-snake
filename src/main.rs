use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;
use bevy::text::Text2dBounds;
use bevy::core::FixedTimestep;
use rand::Rng;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.94, 1.0, 1.0)))
        .add_plugins(DefaultPlugins)
        .add_startup_system(setup)
        .add_system(snake_movement)
        .add_system_set_to_stage(
            CoreStage::PostUpdate,
            SystemSet::new()
                .with_system(position_translation)
                .with_system(maintain_text))
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(1.0))
                .with_system(generate_food),
        )
        .run();
}

#[derive(Component)]
struct SnakeHead;

#[derive(Component)]
struct Food {
    display_time: i32,
}

#[derive(Component)]
struct TextBoard;

#[derive(Component)]
struct Position {
    x: f32,
    y: f32,
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // camera
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    // generate snake head
    commands.spawn_bundle(MaterialMesh2dBundle {
        mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
        transform: Transform::default().with_scale(Vec3::splat(32.)),
        material: materials.add(ColorMaterial::from(Color::RED)),
        ..default()
    })
        .insert(SnakeHead)
        .insert(Position { x: 500., y: 500. });
    // Text
    let font = asset_server.load("fonts/data-latin.ttf");
    let text_style = TextStyle {
        font,
        font_size: 20.0,
        color: Color::DARK_GREEN,
    };
    let text_alignment_top_left = TextAlignment {
        vertical: VerticalAlign::Top,
        horizontal: HorizontalAlign::Right,
    };
    commands.spawn_bundle(Text2dBundle {
        text: Text::with_section(
            "",
            text_style,
            text_alignment_top_left),
        text_2d_bounds: Text2dBounds {
            size: Size::new(300.0, 200.0),
        },
        ..default()
    }).insert(TextBoard);
}

fn position_translation(windows: Res<Windows>, mut q: Query<(&mut Position, &mut Transform)>) {
    let window = windows.get_primary().unwrap();

    for (mut pos, mut transform) in q.iter_mut() {
        if pos.x < 16. {
            pos.x = window.width() - 16.;
        } else if pos.x > window.width() {
            pos.x = 16.;
        }

        if pos.y < 16. {
            pos.y = window.height() - 16.;
        } else if pos.y > window.height() - 16. {
            pos.y = 16.;
        }

        transform.translation = Vec3::new(
            pos.x - window.width() / 2.,
            window.height() / 2. - pos.y,
            0.0,
        );
    }
}

fn snake_movement(keys: Res<Input<KeyCode>>, mut head_position: Query<&mut Position, With<SnakeHead>>) {
    for mut pos in head_position.iter_mut()
    {
        if keys.pressed(KeyCode::W) {
            pos.y -= 10.;
        }
        if keys.pressed(KeyCode::A) {
            pos.x -= 10.;
        }
        if keys.pressed(KeyCode::D) {
            pos.x += 10.;
        }
        if keys.pressed(KeyCode::S) {
            pos.y += 10.;
        }
    }
}

fn maintain_text(
    windows: Res<Windows>,
    mut text_board: Query<(&mut Transform, &mut Text), With<TextBoard>>,
    head_position: Query<&Position, With<SnakeHead>>) {
    let window = windows.get_primary().unwrap();
    let (mut transform, mut text) = text_board.single_mut();
    transform.translation = Vec3::new(
        window.width() / 2. - 10.,
        window.height() / 2. - 10.,
        0.0,
    );
    let pos = head_position.single();
    text.sections[0].value = format!("SnakeHead Position: ({:4},{:4})\nScore: {:24}", pos.x, pos.y, 0);
}

fn generate_food(mut commands: Commands, windows: Res<Windows>) {
    let mut rng = rand::thread_rng();
    let window = windows.get_primary().unwrap();

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::YELLOW_GREEN,
                ..default()
            },
            transform: Transform::default().with_scale(Vec3::splat(24.)),
            ..default()
        })
        .insert(Food { display_time: rng.gen_range(5..15) })
        .insert(Position {
            x: rng.gen_range(10.0..window.width() - 10.),
            y: rng.gen_range(10.0..window.height() - 10.),
        });
}

// fn disappear_food() {}
